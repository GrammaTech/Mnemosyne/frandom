;;; Copyright (C) 2020 GrammaTech, Inc.

(defpackage :frandom/test
  (:nicknames :fr/test)
  (:use :cl :frandom/frandom :stefil)
  (:export test))

(in-package :frandom/test)

(defsuite test)
(in-suite test)

(deftest frandom-state.basic-usage ()
  ;; (FRANDOM frs n) behaves like (RANDOM n)
  (let ((frs (make-frandom-state)))
    (is (typep frs 'frandom-state))
    (is (typep (frandom frs 10) '(integer 0 (10))))
    (is (typep (frandom frs 1.0s0)
               '(short-float 0.0s0 1.0s0)))
    (is (typep (frandom frs 1.0f0)
               '(single-float 0.0f0 1.0f0)))
    (is (typep (frandom frs 1.0d0)
               '(double-float 0.0d0 1.0d0)))
    (is (typep (frandom frs 1.0l0)
               '(long-float 0.0l0 1.0l0)))))

(defun some-randoms (frs)
  (loop repeat 20 collect (frandom frs (ash 1 20))))

;; Equivalent children should behave the same
(deftest spawn-child.same-child-index ()
  (let* ((frs (make-frandom-state))
         (child1 (spawn-child frs 1))
         (child2 (spawn-child frs 1)))
    (is (equal (some-randoms child1)
               (some-randoms child2)))))

;; Interspersed frandom on parent should not
;; affect equivalence of children
(deftest spawn-child.same-spawns-separated ()
  (let* ((frs (make-frandom-state))
         (child1 (spawn-child frs 1)))
    (frandom frs 1.0)
    (let ((child2 (spawn-child frs 1)))
      (is (equal (some-randoms child1)
                 (some-randoms child2))))))

;; Inequivalent children should (almost certainly)
;; behave differently
(deftest spawn-child.different-child-index ()
  (let* ((frs (make-frandom-state))
         (child1 (spawn-child frs 1))
         (child2 (spawn-child frs 2)))
    (is (not (equal (some-randoms child1)
                    (some-randoms child2))))))

;; Children should (almost certainly) behave differently
;; than the parent
(deftest spawn-child.different-from-parent ()
  (let* ((frs (make-frandom-state))
         (child (spawn-child frs 1)))
    (is (not (equal (some-randoms frs)
                    (some-randoms child))))))
