;;; Copyright (C) 2020 GrammaTech, Inc.
(defsystem "frandom"
    :name "frandom"
    :author "GrammaTech"
    :description "Tree structured generation of random generators"
    :long-description "Functionalization of random number generators to
allow generation of trees of random values, where values in separate
subtrees can be modified independently.  This is useful for generating
more easily simplified random inputs for property based testing."
    :depends-on (:frandom/frandom)
    :class :package-inferred-system
    :defsystem-depends-on (:asdf-package-system)
    :perform
    (test-op (o c) (symbol-call :frandom/test '#:test)))
