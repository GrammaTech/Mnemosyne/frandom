;;; Copyright (C) 2020 GrammaTech, Inc.

(defpackage :frandom/frandom
  (:nicknames :frandom)
  (:use :cl :alexandria)
  (:export #:make-frandom-state #:frandom
           #:spawn-child #:frandom-state)
  (:documentation "Main package for frandom system"))
(in-package :frandom)

(defclass frandom-state ()
  ((random-state :accessor rs
                 :type random-state)
   (original-seed :accessor original-seed
                  :type integer)
   (original-random-state :accessor original-random-state
                          :type random-state
                          :initarg :original-random-state
                          :initform (required-argument 'original-random-state)))
  (:documentation "Functionalized version of random-state.  Allows child FRS objects
to be deterministically spawned from this one, with a child index."))

(defmethod slot-unbound ((class t) (frs frandom-state)
                         (slot (eql 'random-state)))
  (setf (rs frs) (make-random-state (original-random-state frs))))

(defun make-frandom-state (&optional (rs *random-state*))
  "Create a FRANDOM-STATE object from RS"
  (make-instance 'frandom-state :original-random-state rs))

(defmethod initialize-instance :after ((frs frandom-state) &key &allow-other-keys)
  (let ((rs (make-random-state (original-random-state frs))))
    (setf (rs frs) rs)
    (setf (original-seed frs) (let ((*random-state* rs)) (random (ash 1 32))))))

(defgeneric frandom (frs n)
  (:documentation "Equivalent to (RANDOM N) on this object.")
  (:method ((frs frandom-state) (n real))
    (let ((*random-state* (rs frs)))
      (random n))))

#+ecl
(defparameter *frs-readtable* (copy-readtable nil))

(defgeneric spawn-child (frs child-index)
  (:documentation "Create a child of a FRS object.  Its state is a deterministic
function of the FRS's original random state and the CHILD-INDEX.")
  (:method ((frs frandom-state) (child-index integer))
    #-(or sbcl ecl) (error "Requires SBCL or ECL")
    #+ecl
    (let* ((s (format nil "#$~a" (+ (original-seed frs) child-index)))
           (new-rs (let ((*readtable* *frs-readtable*))
                     (read-from-string s))))
       (make-frandom-state new-rs))
    #+sbcl
    (let ((new-rs (sb-ext:seed-random-state (+ (original-seed frs) child-index))))
      (make-frandom-state new-rs))))


      

    
