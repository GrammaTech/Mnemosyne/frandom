# frandom

A functionalized random number generator wrapper.

Allow generation of trees of random numbers, in which each random
number is a function of the initial state at its parent and a distinct
label on the edge to each of its children.  The intended use case is
random generation of tree-structured objects, where the contents of
disjoint subtrees can be regenerated independently.